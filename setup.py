#!/usr/bin/env python2

from distutils.core import setup

setup(
		name='dprcon',
		version='1.0',
		description='A simple DarkPlaces RCON client',
		author='Akari`',
		author_email='hetors.email@gmail.com',
		py_modules = ['dprcon']
)
